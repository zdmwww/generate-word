package indi.yun.generate.util;

import com.lowagie.text.Cell;
import com.lowagie.text.Table;

import java.awt.*;

/**
 * @ClassName: WordUtil
 * @Description: Word工具类
 * @Author: Bamboo
 * @Date: 2018/11/9 上午 9:26
 * @Version: 1.0
 */
public class WordUtil {

    /**
     * 
     * @auther: Bamboo
     * @date: 2018/11/09 09:43:46
     * @param table             表格
     * @param titles            标题
     * @param bakeGroundColor   单元格背景颜色
     * @param rowSpan           行合并数
     * @param colSpan           列合并数
     * @return: void
     *
     */
    public static void createTableTitle(Table table, String[] titles, Color bakeGroundColor, int rowSpan, int colSpan){
        for (String title : titles) {
            // 创建列
            Cell cell = new Cell(title);
            cell.setBackgroundColor(bakeGroundColor);
            cell.setHeader(true);
            cell.setColspan(colSpan);
            cell.setRowspan(rowSpan);
            table.addCell(cell);
        }
    }
}
