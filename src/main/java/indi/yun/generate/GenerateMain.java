package indi.yun.generate;

import com.lowagie.text.Font;
import com.lowagie.text.*;
import com.lowagie.text.rtf.RtfWriter2;
import indi.yun.generate.mapper.MysqlTableMapper;
import indi.yun.generate.mapper.OracleTableMapper;
import indi.yun.generate.mapper.TableMapper;
import indi.yun.generate.util.WordUtil;
import org.apache.ibatis.datasource.pooled.PooledDataSource;
import org.apache.ibatis.mapping.Environment;
import org.apache.ibatis.session.Configuration;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.apache.ibatis.transaction.TransactionFactory;
import org.apache.ibatis.transaction.jdbc.JdbcTransactionFactory;

import java.awt.*;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.List;
import java.util.Map;

/**
 * @ClassName: GenerateMain
 * @Description: 自动生成Word文件的数据库设计文档
 * @Author: Bamboo
 * @Date: 2018/11/8 下午 3:06
 * @Version: 1.0
 */
public class GenerateMain {

    /**
     * @param args args
     * @Description: 执行方法
     * @auther: Bamboo
     * @date: 2018/11/08 15:07:25
     * @return: void
     */
    public static void main(String[] args) {
        // 创建Word文档
        createWord(getMapper());
    }

    /**
     * @param tableMapper 获取数据库表信息接口对象
     * @Description: 生成Word文件的数据库设计文档
     * @auther: Bamboo
     * @date: 2018/11/08 16:52:59
     * @return: void
     */
    private static void createWord(TableMapper tableMapper) {
        // 创建文件
        File file = new File("C:\\Users\\lano\\Desktop\\数据库设计文档.doc");
        if (!file.getParentFile().exists()) {
            boolean result = file.getParentFile().mkdirs();
            if (result) {
                System.out.println("文件创建失败");
            }
        }

        // 获取所有表名及其说明
        List<Map<String, Object>> tableList = tableMapper.selectAllTable("enroll");

        // 创建word文档,并设置纸张的大小
        Document document = new Document(PageSize.A4);

        try {
            // 创建word文档
            RtfWriter2.getInstance(document, new FileOutputStream(file));
            document.open();
            // 设置文档标题
            Paragraph title = new Paragraph("数据库表设计文档", new Font(Font.NORMAL, 24, Font.BOLDITALIC, new Color(0, 0, 0)));
            // 设置对齐
            title.setAlignment(1);
            document.add(title);

            // 通过查询出来的表遍历, 生成表格
            for (Map<String, Object> stringObjectMap : tableList) {
                // 表名
                String tableName = (String) stringObjectMap.get("name");
                // 表说明
                String comment = (String) stringObjectMap.get("comment");

                // 标题2:表名及其说明
                Paragraph title2 = new Paragraph("表名：" + tableName + " " + comment + "");

                // 创建有8列的表格
                Table table = new Table(7);
                document.add(new Paragraph(""));
                // 设置表格边框宽度
                table.setBorderWidth(1);
                // 设置表格边框颜色
                table.setBorderColor(Color.BLACK);
                // 设置表格边框填充
                table.setPadding(0);
                // 设置表格间距
                table.setSpacing(0);

                // 设置表格标题
                String[] tableTitle = new String[]{"序号", "字段名", "类型", "是否为空", "默认", "主键", "字段说明"};
                WordUtil.createTableTitle(table, tableTitle, new Color(176, 196, 222), 1, 1);


                // 设置表格内容
                editTable(table, tableMapper, tableName);
                // 写入表说明
                document.add(title2);
                // 写入表格
                document.add(table);
            }

        } catch (DocumentException | FileNotFoundException e) {
            e.printStackTrace();
        } finally {
            document.close();
        }
    }

    /**
     * 编辑表格，设置表格内容
     *
     * @param table       Word表格
     * @param tableMapper 查询接口类
     * @param tableName   数据库表名
     * @auther: Bamboo
     * @date: 2018/11/09 11:12:17
     * @return: void
     */
    private static void editTable(Table table, TableMapper tableMapper, String tableName) {
        // 获取某张表的所有字段说明
        List<Map<String, String>> fieldList = tableMapper.selectFields(tableName);

        // 字段数
        int fieldSize = fieldList.size();
        for (int k = 0; k < fieldSize; k++) {
            Map<String, String> map = fieldList.get(k);
            try {
                // 序号
                table.addCell(String.valueOf(k + 1));
                // 字段名
                table.addCell(map.get("NAME"));
                // 类型
                table.addCell(map.get("DATA_TYPE"));
                // 是否为空
                table.addCell(map.get("ISNULL"));
                // 默认
                table.addCell(map.get("DEFAULT_VALUE"));
                // 主键
                table.addCell(map.get("COLUMN_KEY"));
                // 字段说明
                table.addCell(map.get("COMMENT"));
            } catch (BadElementException e) {
                e.printStackTrace();
            }
        }
    }


    /**
     * @Description: 获取查询表相关信息用的Mapper
     * @auther: Bamboo
     * @date: 2018/11/08 15:55:29
     * @return: indi.yun.generate.mapper.TableMapper
     */
    private static MysqlTableMapper getMapper() {
        // 1、 通过Java API构建SqlSessionFactory
        // 1.1 数据源
        PooledDataSource dataSource = new PooledDataSource();
        dataSource.setDriver("com.mysql.cj.jdbc.Driver");
        dataSource.setUrl("jdbc:mysql://localhost:3306/enroll?serverTimezone=Asia/Shanghai");
        dataSource.setUsername("root");
        dataSource.setPassword("1234567");
        // 1.2 事务管理器
        TransactionFactory transactionFactory = new JdbcTransactionFactory();
        Environment environment = new Environment("dev", transactionFactory, dataSource);
        Configuration configuration = new Configuration(environment);

        // 1.3 注册映射器类所在包名下的所有映射器
        configuration.addMappers("indi.yun.generate.mapper");
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(configuration);

        SqlSession sqlSession = sqlSessionFactory.openSession();
        return sqlSession.getMapper(MysqlTableMapper.class);
    }
}
