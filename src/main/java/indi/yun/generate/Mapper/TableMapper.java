package indi.yun.generate.Mapper;

import org.apache.ibatis.annotations.Select;

import java.util.List;
import java.util.Map;

/**
 * @ClassName: TableMapper
 * @Description: 数据库表映射器接口
 * @Author: Bamboo
 * @Date: 2018/11/8 0008下午 3:23
 * @Version: 1.0
 */
public interface TableMapper {



    /**
     * 查询所有表名及其说明
     *
     * @auther: Bamboo
     * @date: 2018/11/08 15:26:31
     * @param:
     * @return: java.util.List<java.util.Map<java.lang.String,java.lang.Object>> 所有表名及其说明
     *
     */
    @Select("SELECT" +
            "  t.TABLE_NAME AS tableName, " +
            "  f.COMMENTS " +
            "FROM" +
            "  USER_TABLES t " +
            "INNER JOIN USER_TAB_COMMENTS f " +
            "        ON t.TABLE_NAME = f.TABLE_NAME " +
            "ORDER BY " +
            "  t.TABLE_NAME")
    List<Map<String, Object>> selectAllTable();

    /**
     *
     * 查询当前表所有字段
     * 
     * @auther: Bamboo
     * @date: 2018/11/08 17:27:46
     * @param tableName 
     * @return: java.util.List
     *
     */
    List selectFields(String tableName);
}