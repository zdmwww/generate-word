package indi.yun.generate.mapper;

import org.apache.ibatis.annotations.Select;

import java.util.List;
import java.util.Map;

/**
 * @ClassName MysqlTableMapper
 * @Description Mysql数据库表映射器接口
 * @Author Li bw libw@xiaoqiedu.cn
 * @Date 2021/4/16 14:23
 * @Version 1.0
 */
public interface MysqlTableMapper extends TableMapper{

    /**
     * 查询所有表名及其说明
     * @param databaseName  数据库名称
     * @return java.util.List<java.util.Map<java.lang.String,java.lang.Object>>
     * @author Bamboo aspbamboo@gmail.com
     * @date 2021/04/16 14:31:23
     */
    @Override
    @Select("SELECT" +
            "  TABLE_NAME AS name, " +
            "  TABLE_COMMENT AS comment " +
            "FROM" +
            "  INFORMATION_SCHEMA.TABLES " +
            "WHERE" +
            "  TABLE_SCHEMA = #{databaseName};")
    List<Map<String, Object>> selectAllTable(String databaseName);

    /**
     * 查询当前表所有字段
     * @auther: Bamboo
     * @date: 2018/11/08 17:27:46
     * @param tableName 表名
     * @return: java.util.List
     */
    @Override
    @Select("SELECT " +
            "  COLUMN_NAME AS NAME, " +
            "  COLUMN_TYPE AS DATA_TYPE, " +
            "  '' AS DATA_LENGTH, " +
            "  IS_NULLABLE AS ISNULL, " +
            "  COLUMN_KEY AS COLUMN_KEY, " +
            "  COLUMN_COMMENT AS COMMENT, " +
            "  COLUMN_DEFAULT AS DEFAULT_VALUE " +
            "FROM " +
            "  INFORMATION_SCHEMA.COLUMNS " +
            "WHERE " +
            "  TABLE_NAME = #{tableName};")
    List<Map<String, String>> selectFields(String tableName);

}